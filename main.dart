import 'package:flutter/material.dart';
import 'dashboard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'First App',
      home: MyHomePage(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key,}) : super(key: key);




  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor:Colors.black,
      appBar: AppBar(
        
        title: const Text('Login'),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40 ),
          child: Column(
            children: [
              const SizedBox(
                height: 70,
              ),
              _labelTextInput("Username","Enter username here",false),
              const SizedBox(
                height: 70,
              ),
              _labelTextInput("Password","Enter password here",true),
              const SizedBox(
                height: 90,
              ),
              _loginBtn(context),
            ],
          ),
        ), 
      ), 
    );
  }
}

Widget _loginBtn(BuildContext context){
  return Container(
    width: double.infinity,
    height: 60,
    decoration: const BoxDecoration(
      color: Colors.brown,
      borderRadius: BorderRadius.all(Radius.circular(10)),
    ),
    child: TextButton(
      onPressed: () => {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => const Dashboard())),
      },
      child: const Text(
        "Login",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    ),
  );
}


Widget _labelTextInput(String label,String hintText,bool isPassed){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: const TextStyle(
          color: Colors.brown,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      TextField(
        style:const TextStyle(color: Colors.white),
        obscureText: isPassed,
        cursorColor: Colors.white,
        decoration: InputDecoration(
          hintText:hintText,
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize:20,
            fontWeight:FontWeight.w100,
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color:Colors.white
            ),
          ),
        ),
      ),
    ],
  );
}
