import 'package:flutter/material.dart';

class Search extends StatelessWidget{
  const Search({Key? key}): super(key: key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Search For Book'),
        centerTitle: true,
        actions:[
          IconButton(
            onPressed:(){
              showSearch(
                context:context,
                delegate:MySearchDelegate(),
              );
            },
            icon: const Icon(Icons.search),
          ),
        ],
        backgroundColor: Colors.brown,
      ),
    );
  }    
}

class MySearchDelegate extends SearchDelegate{

  @override
  List<Widget> buildActions(BuildContext context){
    return[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          if(query==''){
            close(context,null);
          }
          query='';
        },
      ),
    ]; 
  }

  @override
  Widget buildLeading(BuildContext context){
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context,null);
      },
    );

  }

   @override
  Widget buildResults(BuildContext context){
    return Scaffold();
  }

   @override
  Widget buildSuggestions(BuildContext context){
    return Scaffold();
  }
}

class Edit extends StatelessWidget{
  const Edit({Key? key}): super(key: key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Edit Profile'),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      body:Container(
        child:ListView(
          children:[
            Center(
              child:Stack(
                children:[
                  Container(
                    width:130,
                    height:130,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image:DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage("https://cdn.pixabay.com/photo/2018/11/13/21/43/avatar-3814049_960_720.png"),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child:Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color:Colors.brown,
                      ),
                      child: const Icon(Icons.edit,color:Colors.white,),
                    ),
                  ),
                ],  
              ),
            ),
             const SizedBox(
                height: 20,
              ),
              _labelTextInput("Full Name", "Name Surname", false),
               const SizedBox(
                height: 70,
              ),
               _labelTextInput("E-mail", "123456@example.com", false),
               const SizedBox(
                height: 70,
              ),
               _labelTextInput("Username", "HotBun69", false),
               const SizedBox(
                height: 70,
              ),
               _labelTextInput("Password", "!eR78", true),
               const SizedBox(
                height: 100,
              ),
              Row(
                children:[
                  OutlinedButton(
                     onPressed:(){},
                    child: const Text(
                      "Cancel",
                      style: TextStyle(
                        fontSize:14.0,
                        letterSpacing:2.2,
                        color:Colors.white,
                      ),
                    ),
                    //style:ButtonStyle(
                      //padding: const EdgeInsets.all(50),
                      //shape:RoundedRectangleBorder(
                        //borderRadius: BorderRadius.circular(20),
                      //), 
                  ),
                  ElevatedButton(
                    onPressed:(){} ,
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      onPrimary: Colors.white,
                    ),
                    child:const Text(
                      "Save",
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing:2.2,
                        color:Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ],
        ),
      ),
    );
  }  
}


Widget _labelTextInput(String label,String hintText,bool isPassed){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: const TextStyle(
          color: Colors.brown,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      TextField(
        style:const TextStyle(color: Colors.white),
        obscureText: isPassed,

        cursorColor: Colors.white,
        decoration: InputDecoration(
          hintText:hintText,
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize:20,
            fontWeight:FontWeight.w100,
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color:Colors.white
            ),
          ),
        ),
      ),
    ],
  );
}





class Chat extends StatelessWidget{
  const Chat({Key? key}): super(key:key);
  
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat With Assistant'),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      body: Column(
        children: [
          Expanded(child:Container()),
          Container(
            color:Colors.grey,
            child: const TextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(12),
                hintText: 'Type your message here...',
              ),
            ),
          ),
        ],      
      ),
    );
  }  
}




class Operation extends StatelessWidget{
  const Operation({Key? key}): super(key:key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Operating Times'),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      body:const Text(
        "Monday     8:20am-11:55pm\n\n Tuesday      8:20am-11:55pm\n\n Wednesday      8:20am-11:55pm\n\n Thursday     8:20am-11:55pm\n\n Friday     8:20am-8pm\n\n Saturday     9am-1pm\n\n Sunday      closed\n\n",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
        ),
      ),
    );
  }
}